package service;

import exception.LimitExceededException;
import exception.OverdraftException;
import exception.UnavailableFundsException;
import model.CheckingAccount;
import model.Owner;
import org.junit.Test;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CheckingAccountOperationTest {

    private CheckingAccountOperation checkingAccountOperation = new CheckingAccountOperation();

    @Test
    public void withdrawal_limit100Amount50Balance500_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final CheckingAccount checkingAccount = new CheckingAccount(owner, valueOf(500), valueOf(100));

        checkingAccountOperation.withdrawal(checkingAccount, valueOf(50));

        assertThat("the requested amount should be subtracted from balance", checkingAccount.getBalance(), equalTo(valueOf(450)));
    }

    @Test
    public void withdrawal_limit0Amount0Balance0_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final CheckingAccount checkingAccount = new CheckingAccount(owner, ZERO, ZERO);

        checkingAccountOperation.withdrawal(checkingAccount, ZERO);

        assertThat("the requested amount should remain intact", checkingAccount.getBalance(), equalTo(ZERO));
    }

    @Test
    public void withdrawal_limit100Amount150Balance100_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final CheckingAccount checkingAccount = new CheckingAccount(owner, valueOf(100), valueOf(100));

        checkingAccountOperation.withdrawal(checkingAccount, valueOf(150));

        assertThat("the requested amount should be subtracted from balance", checkingAccount.getBalance(), equalTo(valueOf(-50)));
    }

    @Test
    public void withdrawal_limit100Amount200Balance100_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final CheckingAccount checkingAccount = new CheckingAccount(owner, valueOf(100), valueOf(100));

        checkingAccountOperation.withdrawal(checkingAccount, valueOf(200));

        assertThat("the requested amount should be subtracted from balance", checkingAccount.getBalance(), equalTo(valueOf(-100)));
    }

    @Test(expected = UnavailableFundsException.class)
    public void withdrawal_limit0Amount100Balance50_expectUnavailableFundsException() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final CheckingAccount checkingAccount = new CheckingAccount(owner, valueOf(50), ZERO);

        checkingAccountOperation.withdrawal(checkingAccount, valueOf(100));
    }

    @Test(expected = LimitExceededException.class)
    public void withdrawal_limit100Amount600Balance50_expectLimitException() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final CheckingAccount checkingAccount = new CheckingAccount(owner, valueOf(50), valueOf(100));

        checkingAccountOperation.withdrawal(checkingAccount, valueOf(600));
    }

    @Test
    public void transfer_fromBalance500ToBalance500Amount100Limit0_expectTransfer() throws OverdraftException {

        final CheckingAccount fromCheckingAccount = new CheckingAccount(
                new Owner("Gabriel", "Saratura"),
                valueOf(500),
                ZERO
        );

        final CheckingAccount toCheckingsAccount = new CheckingAccount(
                new Owner("John", "Smith"),
                valueOf(500),
                ZERO
        );

        checkingAccountOperation.transfer(fromCheckingAccount, toCheckingsAccount, valueOf(100));

        assertThat("the requested amount should be subtracted from account after transfer", fromCheckingAccount.getBalance(), equalTo(valueOf(400)));
        assertThat("the requested amount should be added to account after transfer", toCheckingsAccount.getBalance(), equalTo(valueOf(600)));
    }

    @Test
    public void transfer_fromBalance0ToBalance0Amount100Limit100_expectTransfer() throws OverdraftException {

        final CheckingAccount fromCheckingAccount = new CheckingAccount(
                new Owner("Gabriel", "Saratura"),
                ZERO,
                valueOf(100)
        );

        final CheckingAccount toCheckingAccount = new CheckingAccount(
                new Owner("John", "Smith"),
                ZERO,
                ZERO
        );

        checkingAccountOperation.transfer(fromCheckingAccount, toCheckingAccount, valueOf(100));

        assertThat("the requested amount should be subtracted from account after transfer", fromCheckingAccount.getBalance(), equalTo(valueOf(-100)));
        assertThat("the requested amount should be added to account after transfer", toCheckingAccount.getBalance(), equalTo(valueOf(100)));
    }

    @Test(expected = UnavailableFundsException.class)
    public void transfer_fromBalance500ToBalance500Amount600Limit0_expectTransfer() throws OverdraftException {

        final CheckingAccount fromCheckingAccount = new CheckingAccount(
                new Owner("Gabriel", "Saratura"),
                valueOf(500),
                ZERO
        );

        final CheckingAccount toCheckingAccount = new CheckingAccount(
                new Owner("John", "Smith"),
                valueOf(500),
                ZERO
        );

        checkingAccountOperation.transfer(fromCheckingAccount, toCheckingAccount, valueOf(600));
    }
}
