package service;

import exception.OverdraftException;
import exception.UnavailableFundsException;
import model.BankAccount;
import model.Owner;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class BankOperationTest {

    private BankOperation<BankAccount> bankOperation = new BankOperation<BankAccount>() {
    };

    @Test
    public void deposit_Balance0Amount100_expectDeposit() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final GenericBankAccount genericBankAccount = new GenericBankAccount(owner, ZERO);

        bankOperation.deposit(genericBankAccount, valueOf(100));

        assertThat("deposit operation should be successful", genericBankAccount.getBalance(), equalTo(valueOf(100)));
    }

    @Test
    public void deposit_Balance100Amount100_expectNoModifications() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final GenericBankAccount genericBankAccount = new GenericBankAccount(owner, valueOf(100));

        bankOperation.deposit(genericBankAccount, valueOf(100));

        assertThat("the balance should remain intact", genericBankAccount.getBalance(), equalTo(valueOf(200)));
    }

    @Test
    public void withdrawal_Balance100Amount100_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final GenericBankAccount genericBankAccount = new GenericBankAccount(owner, valueOf(100));

        bankOperation.withdrawal(genericBankAccount, valueOf(100));

        assertThat("withdrawal operation should be successful", genericBankAccount.getBalance(), equalTo(ZERO));
    }

    @Test
    public void withdrawal_Balance200Amount100_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        ;
        final GenericBankAccount genericBankAccount = new GenericBankAccount(owner, valueOf(200));

        bankOperation.withdrawal(genericBankAccount, valueOf(100));

        assertThat("withdrawal operation should be successful", genericBankAccount.getBalance(), equalTo(valueOf(100)));
    }

    @Test
    public void withdrawal_Balance0Amount0_expectWithdrawal() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        ;
        final GenericBankAccount genericBankAccount = new GenericBankAccount(owner, ZERO);

        bankOperation.withdrawal(genericBankAccount, ZERO);

        assertThat("withdrawal operation should be successful", genericBankAccount.getBalance(), equalTo(ZERO));
    }

    @Test(expected = UnavailableFundsException.class)
    public void withdrawal_Balance0Amount200_expectUnavailableFundsException() throws OverdraftException {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final GenericBankAccount genericBankAccount = new GenericBankAccount(owner, ZERO);

        bankOperation.withdrawal(genericBankAccount, valueOf(200));
    }

    private class GenericBankAccount extends BankAccount {
        GenericBankAccount(final Owner owner, final BigDecimal balance) {
            super(owner, balance);
        }
    }
}
