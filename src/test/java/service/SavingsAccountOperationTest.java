package service;

import model.Owner;
import model.SavingsAccount;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class SavingsAccountOperationTest {

    private SavingsAccountOperation savingsAccountOperation = new SavingsAccountOperation();

    @Test
    public void calculateInterestRate_interest10Balance500_expectCalculation() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final SavingsAccount savingsAccount = new SavingsAccount(owner, valueOf(500), valueOf(10));

        final BigDecimal interest = savingsAccountOperation.calculateInterestRate(savingsAccount);

        assertThat("the saving account should have 50 interest", interest, equalTo(valueOf(50)));
    }

    @Test
    public void calculateInterestRate_interest10Balance0_expectCalculation() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final SavingsAccount savingsAccount = new SavingsAccount(owner, ZERO, valueOf(10));

        final BigDecimal interest = savingsAccountOperation.calculateInterestRate(savingsAccount);

        assertThat("the saving account should have 50 interest", interest, equalTo(ZERO));
    }

    @Test
    public void payInterestRate_interest20Balance300_expectPayedInterests() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final SavingsAccount savingsAccount = new SavingsAccount(owner, valueOf(300), valueOf(20));

        savingsAccountOperation.payInterestRate(savingsAccount);

        assertThat("the saving account should have payed interests", savingsAccount.getBalance(), equalTo(valueOf(360)));
    }

    @Test
    public void payInterestRate_interest0Balance300_expectNoInterests() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final SavingsAccount savingsAccount = new SavingsAccount(owner, valueOf(300), ZERO);

        savingsAccountOperation.payInterestRate(savingsAccount);

        assertThat("the saving account should remain intact", savingsAccount.getBalance(), equalTo(valueOf(300)));
    }

    @Test
    public void payInterestRate_interest0Balance0_expectNoInterests() {

        final Owner owner = new Owner("Gabriel", "Saratura");
        final SavingsAccount savingsAccount = new SavingsAccount(owner, ZERO, ZERO);

        savingsAccountOperation.payInterestRate(savingsAccount);

        assertThat("the saving account should remain intact", savingsAccount.getBalance(), equalTo(ZERO));
    }
}
