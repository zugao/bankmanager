package exception;

import java.math.BigDecimal;

public class LimitExceededException extends OverdraftException {

    public LimitExceededException(final BigDecimal amount, final BigDecimal balance, final BigDecimal limit) {
        super(amount, balance, limit);
    }
}
