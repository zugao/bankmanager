package exception;

import java.math.BigDecimal;

public class UnavailableFundsException extends OverdraftException {

    public UnavailableFundsException(final BigDecimal request, final BigDecimal balance) {
        super(request, balance);
    }
}
