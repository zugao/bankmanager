package exception;

import java.math.BigDecimal;

import static java.lang.String.format;

public class OverdraftException extends Exception {

    public OverdraftException(final BigDecimal request, final BigDecimal balance) {
        super(format("cannot withdraw {}, available {}", request, balance));
    }

    public OverdraftException(final BigDecimal amount, final BigDecimal balance, final BigDecimal limit) {
        super(format("cannot withdraw {}, available {}", amount, balance.add(limit)));
    }
}
