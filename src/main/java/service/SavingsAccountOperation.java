package service;

import model.SavingsAccount;

import java.math.BigDecimal;

import static java.math.BigDecimal.valueOf;
import static java.util.Objects.requireNonNull;

public class SavingsAccountOperation implements BankOperation<SavingsAccount> {

    public BigDecimal calculateInterestRate(final SavingsAccount savingsAccount) {

        requireNonNull(savingsAccount, "savingsAccount must not be null");

        return savingsAccount.getBalance().multiply(savingsAccount.getInterestRate())
                .divide(valueOf(100), 2);
    }

    public void payInterestRate(final SavingsAccount savingsAccount) {

        requireNonNull(savingsAccount, "savingsAccount must not be null");

        savingsAccount.setBalance(savingsAccount.getBalance().add(calculateInterestRate(savingsAccount)));
    }
}
