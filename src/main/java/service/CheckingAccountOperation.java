package service;

import exception.LimitExceededException;
import exception.OverdraftException;
import model.CheckingAccount;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.util.Objects.requireNonNull;

public class CheckingAccountOperation implements BankOperation<CheckingAccount> {

    @Override
    public void withdrawal(
            final CheckingAccount checkingAccount,
            final @DecimalMin("0") BigDecimal amount) throws OverdraftException {

        requireNonNull(checkingAccount, "checkingAccount must not be null");
        requireNonNull(amount, "amount must not be null");

        final BigDecimal balance = checkingAccount.getBalance();
        final BigDecimal limit = checkingAccount.getLimit();

        if (limit.compareTo(ZERO) == 0) {

            BankOperation.super.withdrawal(checkingAccount, amount);

        } else {

            if (balance.add(limit).compareTo(amount) < 0) {

                throw new LimitExceededException(amount, balance, limit);

            }
        }

        checkingAccount.setBalance(balance.subtract(amount));
    }

    public void transfer(
            final CheckingAccount fromCheckingAccount,
            final CheckingAccount toCheckingAccount,
            final @DecimalMin("0") BigDecimal amount) throws OverdraftException {

        requireNonNull(fromCheckingAccount, "fromCheckingAccount must not be null");
        requireNonNull(toCheckingAccount, "toCheckingAccount must not be null");
        requireNonNull(amount, "amount must not be null");

        this.withdrawal(fromCheckingAccount, amount);
        toCheckingAccount.setBalance(toCheckingAccount.getBalance().add(amount));
    }
}
