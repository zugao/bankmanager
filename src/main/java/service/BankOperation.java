package service;

import exception.OverdraftException;
import exception.UnavailableFundsException;
import model.BankAccount;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

public interface BankOperation<T extends BankAccount> {

    default void deposit(
            final T bankAccount,
            final @DecimalMin("0") BigDecimal amount) {

        requireNonNull(bankAccount, "bankAccount must not be null");
        requireNonNull(amount, "amount must not be null");

        bankAccount.setBalance(bankAccount.getBalance().add(amount));
    }

    default void withdrawal(
            final T bankAccount,
            final @DecimalMin("0") BigDecimal amount) throws OverdraftException {

        requireNonNull(bankAccount, "bankAccount must not be null");
        requireNonNull(amount, "amount must not be null");

        final BigDecimal balance = bankAccount.getBalance();

        if (balance.compareTo(amount) < 0) {
            throw new UnavailableFundsException(amount, balance);
        }

        bankAccount.setBalance(balance.subtract(amount));
    }
}
