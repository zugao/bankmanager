package model;

import javax.validation.constraints.NotNull;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class Owner {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    public Owner(final String firstName, final String lastName) {

        requireNonNull(firstName, "owner must not be null");
        requireNonNull(lastName, "owner must not be null");

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Owner owner = (Owner) o;

        return Objects.equals(firstName, owner.firstName) &&
                Objects.equals(lastName, owner.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }
}
