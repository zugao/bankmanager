package model;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public abstract class BankAccount {

    @NotNull
    private Owner owner;

    @NotNull
    private BigDecimal balance;

    public BankAccount(final Owner owner, final BigDecimal balance) {

        requireNonNull(owner, "owner must not be null");
        requireNonNull(balance, "balance must not be null");

        this.owner = owner;
        this.balance = balance;
    }

    public Owner getOwner() {
        return owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(final BigDecimal balance) {

        requireNonNull(balance, "balance must not be null");

        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BankAccount that = (BankAccount) o;

        return Objects.equals(owner, that.owner) &&
                Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }
}
