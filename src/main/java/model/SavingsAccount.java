package model;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class SavingsAccount extends BankAccount {

    @NotNull
    @DecimalMin("0")
    @DecimalMax("100")
    @Digits(integer = 3, fraction = 2)
    private BigDecimal interestRate;

    public SavingsAccount(
            final Owner owner,
            final BigDecimal balance,
            final BigDecimal interestRate) {

        super(owner, balance);

        requireNonNull(interestRate, "interestRate must not be null");

        this.interestRate = interestRate;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(final BigDecimal interestRate) {

        requireNonNull(interestRate, "interestRate must not be null");

        this.interestRate = interestRate;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        SavingsAccount that = (SavingsAccount) o;

        return Objects.equals(interestRate, that.interestRate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), interestRate);
    }
}
