package model;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class CheckingAccount extends BankAccount {

    @NotNull
    @DecimalMin("0")
    private BigDecimal limit;

    public CheckingAccount(
            final Owner owner,
            final BigDecimal balance,
            final BigDecimal limit) {

        super(owner, balance);

        requireNonNull(limit, "limit must not be null");

        this.limit = limit;
    }

    public BigDecimal getLimit() {
        return limit;
    }

    public void setLimit(final BigDecimal limit) {

        requireNonNull(limit, "limit must not be null");

        this.limit = limit;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        CheckingAccount that = (CheckingAccount) o;

        return Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), limit);
    }
}
